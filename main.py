#!/usr/bin/env python3

import socketio
import base64
import cv2
import twitch
import time
from gpiozero import Motor
from gpiozero import OutputDevice


RUNNING = False
CAM = None
QUALITY = 80


# Set up socket listener
sio = socketio.Client()


# Assign GPIO pins to commands
GPIO_M1_FORWARD = 22
GPIO_M1_BACK    = 27
GPIO_M1_SPEED   = 17
GPIO_M2_FORWARD = 23
GPIO_M2_BACK    = 18
GPIO_M2_SPEED   = 24
GPIO_STANDBY    = 25

MOTOR_RIGHT_SPEED = 0.97
MOTOR_LEFT_SPEED = 1


# Declare output devices
STANDBY = OutputDevice(GPIO_STANDBY)
M1_SPEED = OutputDevice(GPIO_M1_SPEED)
M2_SPEED = OutputDevice(GPIO_M2_SPEED)

MOTOR_RIGHT = Motor(GPIO_M1_FORWARD, GPIO_M1_BACK)
MOTOR_LEFT = Motor(GPIO_M2_FORWARD, GPIO_M2_BACK)


def main():
    # Connect to socket
    sio.connect('https://munieradubery.com', socketio_path='/play-with-cats-api/socket.io', namespaces=['/rat'])
    print('sid: ', sio.sid)

    # Prepare motors
    STANDBY.on() 
    M1_SPEED.on()
    M2_SPEED.on()

    # Comment out when not using twitch integration
    connect_twitch()


# connect to twitch chat
def connect_twitch():
    helix = twitch.Helix('m2uo9zdxbpyvs3knh0tzdwkq64ovot', 'gqpbt7wzvjxe67rcrwz6lnccmf3nzg')
    twitch.Chat(channel='#minbiscuit', nickname='bot', oauth='oauth:fijwdf7fy1u1oo26tvzxm1aprgh6er').subscribe(handle_chat)


# Read twitch chat messages and execute commands
def handle_chat(message):
    if message.text == 'up':
        handle_forward()
        time.sleep(0.5)
        handle_stop()
    elif message.text == 'down':
        handle_backward()
        time.sleep(0.5)
        handle_stop()
    elif message.text == 'left':
        handle_left()
        time.sleep(0.2)
        handle_stop()
    elif message.text == 'right':
        handle_right()
        time.sleep(0.2)
        handle_stop()


############### Movement commands ###############
@sio.on('forward', namespace='/rat')
def handle_forward():
    MOTOR_RIGHT.forward(MOTOR_RIGHT_SPEED)
    MOTOR_LEFT.forward(MOTOR_LEFT_SPEED)


@sio.on('left', namespace='/rat')
def handle_left():
    MOTOR_RIGHT.forward()
    MOTOR_LEFT.backward()


@sio.on('right', namespace='/rat')
def handle_right():
    MOTOR_RIGHT.backward()
    MOTOR_LEFT.forward()


@sio.on('backward', namespace='/rat')
def handle_backward():
    MOTOR_RIGHT.backward()
    MOTOR_LEFT.backward()


@sio.on('stop', namespace='/rat')
def handle_stop():
    MOTOR_RIGHT.stop()
    MOTOR_LEFT.stop()


# Send frame on server response
@sio.event(namespace='/rat')
def ready():
    emit_frame()


# Read and encode frame to base64
# then emit to socket
def emit_frame():
    if RUNNING:
        try:
            _, frame = CAM.read()
            frame = cv2.flip(frame, 0)
            frame = cv2.flip(frame, 1)
            _, frame = cv2.imencode('.jpg', frame, [int(cv2.IMWRITE_JPEG_QUALITY), QUALITY])
            frame = base64.b64encode(frame.tobytes()).decode()
            sio.emit('rat_frame', {'frame': frame}, namespace='/rat')
        except:
            pass

# Event from server indicates if a client watching the stream. 
# Stop emitting frames if no client is connected.
# If a client connects, restart frame emit loop.
@sio.event(namespace='/rat')
def video(json):
    global RUNNING, CAM
    if json['stream']:
        CAM = cv2.VideoCapture(0)
        CAM.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
        CAM.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)
        RUNNING = True
        emit_frame()
    else:
        RUNNING = False
        CAM.release()

main()
