# Play With Cats
## What is Play With Cats?
Play with cats is a project with the goal of increasing cat shelter adoptions and donations.
Inspired by Twitch Plays Pokemon, users control a robot mouse toy over the internet to play with cats. The mouse can be controlled by individuals via the website, or by a huge number of users with twitch streaming integration.
The users can use the two camera streams (one from the robots perspective and one from overhead) to manipulate the toy and watch the cats reactions.

## This sounds cool! Can I see a working demo?
Sure! There was a test stream which can be viewed here: https://www.twitch.tv/videos/804899977?t=00h07m20s

And an informational video which can be viewed here: https://youtu.be/HWtyuBZOBvQ

## So what does this code do?
This code is for controlling the robot rat and parsing frames back to the backend. The code also includes the Twitch Bot code that will read user messages and execute those movements. Note the Twitch tokens within this code have been revoked.

## Is this site currently live?
Not at the moment! But I may set it up again some time in the future :)
